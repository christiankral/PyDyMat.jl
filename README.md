# PyDyMat

Simple interface to [https://www.j-raedler.de/projects/dymat/](DyMat) to read OpenModelica and Dymola result files in Matlab format.